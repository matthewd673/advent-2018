using System;
using System.Collections.Generic;
using System.IO;

class Program
{

	static String input = "";

	public static void Main (string[] args)
	{
		input = File.ReadAllText("input.txt");
		
		//partOne();
		partTwo();

	}

	public static void partOne()
	{
		String[] ids = input.Split('\n');

		Dictionary<int, int> tally = new Dictionary<int, int>();

		foreach(String id in ids)
		{
			char[] letters = id.ToCharArray();

			Dictionary<char, int> tracker = new Dictionary<char, int>();

			foreach(char l in letters)
			{
				try
				{
					tracker.Add(l, 1);
				}
				catch (ArgumentException)
				{
					tracker[l]++;
				}
			}

			List<char> checkedLetters = new List<char>();
			List<int> registeredCts = new List<int>();

			foreach(char l in letters)
			{
				if(!checkedLetters.Contains(l) && !registeredCts.Contains(tracker[l]))
				{
					int ct = tracker[l];
					try
					{
						tally.Add(ct, 1);
					}
					catch (ArgumentException)
					{
						tally[ct]++;
					}
					checkedLetters.Add(l);
					registeredCts.Add(ct);
				}
			}

		}

		Console.WriteLine(tally[2] + " " + tally[3] + " " + (tally[2] * tally[3]));
	}

	public static void partTwo()
	{
		String[] ids = input.Split('\n');

		for(int i = 0; i < ids.Length; i++)
		{
			for(int j = 0; j < ids.Length; j++)
			{
				if(i != j)
				{
					String matched = compareStrings(ids[i], ids[j]);
					if(matched.Length == ids[i].Length - 1)
					{
						Console.WriteLine("s1: {0}", ids[i]);
						Console.WriteLine("s2: {0}", ids[j]);
						Console.WriteLine("a:  {0}", matched);
					}
				}
			}
		}

	}

	public static String compareStrings(String s1, String s2)
	{
		String common = "";

		int mismatches = 0;

		char[] c1 = s1.ToCharArray();
		char[] c2 = s2.ToCharArray();

		for(int i = 0; i < c1.Length; i++) //they are all the same length
		{
			if(c1[i] == c2[i])
				common += c1[i];
			else
				mismatches++;
			if(mismatches > 1)
				break;
		}

		return common;

	}

}