﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AdventDay6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        static string input;
        static int pointOffset = 2500;

        Tile[,] map;
        int width = 0;
        int height = 0;

        bool drawMap = false;

        private void Form1_Load(object sender, EventArgs e)
        {
            input = File.ReadAllText("input.txt");
            //partOne();
        }

        void partOne()
        {

            String[] coordList = input.Split('\n');

            List<Vec2> coords = new List<Vec2>();
            List<Vec2> infinite = new List<Vec2>();

            foreach(String s in coordList)
            {
                coords.Add(getPos(s));
            }

            //bool[,] seen = new bool[coords.Count, coords.Length];

            //int[] count = new int[coords.Length];
            //int maxCount = 0;

            int left = 0;
            int right = 0;
            int top = 0;
            int bottom = 0;

            for(int i = 0; i < coords.Count; i++)
            {
                Vec2 v = coords[i];
                bool farLeft = true, farRight = true, farUp = true, farDown = true;

                foreach(Vec2 c in coords)
                {
                    if (c.x < v.x)
                        farLeft = false;
                    if (c.x > v.x)
                        farRight = false;
                    if (c.y < v.y)
                        farUp = false;
                    if (c.y > v.y)
                        farDown = false;
                }

                if(farLeft || farRight || farUp || farDown)
                {
                    //output.AppendText(v.x + ", " + v.y + "  -  infinite" + System.Environment.NewLine);
                    infinite.Add(v);
                }

                if (farLeft)
                    left = v.x;
                if (farRight)
                    right = v.x;
                if (farUp)
                    top = v.y;
                if (farDown)
                    bottom = v.y;

            }

            //output.Text += left.ToString() + " - > " + right.ToString() + "  &  " + top.ToString() + " -> " + bottom.ToString();

            width = right - left;
            height = bottom - top;

            drawSurface.Width = width;
            drawSurface.Height = height;

            Text = width + "x" + height;

            map = new Tile[width, height];

            for(int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    map[i, j].minDistance = 100000;
                    map[i, j].totalDistance = 0;
                }
            }

            for(int i = 0; i < width; i++)
            {
                for(int j = 0; j < height; j++)
                {
                    for (int k = 0; k < coords.Count; k++)
                    {
                        int distance = getDistance(new Vec2(i, j), new Vec2(coords[k].x - left, coords[k].y - top));

                        map[i, j].totalDistance += distance;

                        if(distance < map[i, j].minDistance)
                        {
                            map[i, j].minDistance = distance;
                            map[i, j].parent = k;
                            //output.Text += "," + i.ToString() + "-" + j.ToString() + ":" + map[i, j].parent.ToString();
                            //Text = map[i, j].parent.ToString();
                        }
                        if(distance == map[i, j].minDistance && k != map[i, j].parent)
                        {
                            map[i, j].parent = -1;
                        }
                    }
                }
                //solveBar.Value = (i / width);
            }

            Text = "Calculation complete!";

            int[] count = new int[coords.Count];
            int maxCount = 0;
            int safeCount = 0;
            
            foreach(Tile t in map)
            {
                if (t.parent > -1)
                {
                    count[t.parent]++;
                    if (count[t.parent] > maxCount)
                        maxCount = count[t.parent];
                }
                if (t.totalDistance < 10000)
                    safeCount++;
            }

            output.AppendText("Far area: " + maxCount);
            output.AppendText(System.Environment.NewLine + "Safe area: " + safeCount);

            drawMap = true;
            drawSurface.Invalidate();

        }

        Vec2 getPos(String coord)
        {
            String[] pos = coord.Split(',');
            int x = Convert.ToInt32(pos[0]);
            int y = Convert.ToInt32(pos[1].Remove(0, 1));

            return new Vec2(x, y);
        }

        int getDistance(Vec2 a, Vec2 b)
        {
            return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
        }

        public struct Vec2
        {
            public int x;
            public int y;

            public Vec2(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public struct Tile
        {
            public int minDistance;
            public int totalDistance;
            public int parent;

            public Tile(int parent = -1) //since the constructor can't be empty
            {
                minDistance = 100000;
                totalDistance = 0;
                this.parent = -1;
            }

        }

        private void drawSurface_Paint(object sender, PaintEventArgs e)
        {

            Graphics g = drawSurface.CreateGraphics();

            Color[] drawColors = new Color[50];
            for (int i = 0; i < drawColors.Length; i++)
            {
                drawColors[i] = Color.FromArgb(255, i * 4, i * 4, i * 4);
            }

            Text = drawMap.ToString() + " " + width.ToString() + "x" + height.ToString();

            if (drawMap)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        if(map[i, j].parent > -1)
                            g.DrawRectangle(new Pen(drawColors[map[i, j].parent]), new Rectangle(i, j, 1, 1));
                        else
                            g.DrawRectangle(Pens.Red, new Rectangle(i, j, 1, 1));
                        if (map[i, j].totalDistance < 10000)
                            g.DrawRectangle(new Pen(Color.FromArgb(130, Color.Green)), new Rectangle(i, j, 1, 1));
                    }
                }
            }
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            drawSurface.Visible = true;
            drawSurface.Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Thread runThread = new Thread(new ThreadStart(partOne));
            //runThread.Start();
            partOne();
        }
    }
}
