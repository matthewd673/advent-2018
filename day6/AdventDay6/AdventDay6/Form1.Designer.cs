﻿namespace AdventDay6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.output = new System.Windows.Forms.TextBox();
            this.drawSurface = new System.Windows.Forms.Panel();
            this.drawButton = new System.Windows.Forms.Button();
            this.solveButton = new System.Windows.Forms.Button();
            this.solveBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // output
            // 
            this.output.Location = new System.Drawing.Point(12, 12);
            this.output.Multiline = true;
            this.output.Name = "output";
            this.output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.output.Size = new System.Drawing.Size(225, 426);
            this.output.TabIndex = 0;
            // 
            // drawSurface
            // 
            this.drawSurface.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.drawSurface.Location = new System.Drawing.Point(243, 12);
            this.drawSurface.Name = "drawSurface";
            this.drawSurface.Size = new System.Drawing.Size(338, 284);
            this.drawSurface.TabIndex = 1;
            this.drawSurface.Paint += new System.Windows.Forms.PaintEventHandler(this.drawSurface_Paint);
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(243, 415);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(75, 23);
            this.drawButton.TabIndex = 2;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // solveButton
            // 
            this.solveButton.Location = new System.Drawing.Point(243, 386);
            this.solveButton.Name = "solveButton";
            this.solveButton.Size = new System.Drawing.Size(75, 23);
            this.solveButton.TabIndex = 3;
            this.solveButton.Text = "SOLVE!";
            this.solveButton.UseVisualStyleBackColor = true;
            this.solveButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // solveBar
            // 
            this.solveBar.Location = new System.Drawing.Point(324, 386);
            this.solveBar.Name = "solveBar";
            this.solveBar.Size = new System.Drawing.Size(257, 23);
            this.solveBar.Step = 1;
            this.solveBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.solveBar.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 450);
            this.Controls.Add(this.solveBar);
            this.Controls.Add(this.solveButton);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.drawSurface);
            this.Controls.Add(this.output);
            this.Name = "Form1";
            this.Text = "Day 6 Visualized";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox output;
        private System.Windows.Forms.Panel drawSurface;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.Button solveButton;
        private System.Windows.Forms.ProgressBar solveBar;
    }
}

