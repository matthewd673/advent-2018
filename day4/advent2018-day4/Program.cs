﻿using System;
using System.Collections.Generic;
using System.IO;

namespace advent2018_day4
{
    class Program
    {

        static String input;

        const int BEGINS = 0;
        const int SLEEPS = 1;
        const int WAKES = 2;

        static Log[] history;

        static List<Guard> guards = new List<Guard>();

        static void Main(string[] args)
        {
            input = File.ReadAllText("input.txt");
            partOne();
        }

        //turns out this can solve part two as well
        static void partOne()
        {

            String[] lines = input.Split('\n');

            history = new Log[lines.Length];

            for(int i = 0; i < history.Length; i++)
            {

                String line = lines[i];

                String[] sLine = line.Split(' ');

                String[] dateString = sLine[0].Remove(0, 1).Split("-");
                String[] timeString = sLine[1].Remove(sLine[1].Length - 1, 1).Split(":");

                DateTime logDate = new DateTime(Convert.ToInt32(dateString[0]), //y
                    Convert.ToInt32(dateString[1]), //m
                    Convert.ToInt32(dateString[2]), //d
                    Convert.ToInt32(timeString[0]), //h
                    Convert.ToInt32(timeString[1]), //m
                    0); //s
                
                int guardId = -1;
                int state = -1;
                if(line.Contains("Guard"))
                {
                    guardId = Convert.ToInt32(line.Split(" ")[3].Remove(0, 1));
                    state = BEGINS;
                }
                else if(line.Contains("falls"))
                    state = SLEEPS;
                else if(line.Contains("wakes"))
                    state = WAKES;

                history[i] = new Log(logDate, guardId, state);

            }

            Array.Sort<Log>(history, (x,y) => x.timestamp.CompareTo(y.timestamp));

            for(int i = 0; i < history.Length; i++)
            {

                Log l = history[i];

                bool newGuard = true;

                if(l.guard == -1)
                {
                    history[i].guard = history[i - 1].guard;
                    l = history[i];

                    for(int k = 0; k < guards.Count; k++)
                    {
                        if(guards[k].id == history[i].guard)
                        {
                            //thats the right one
                            if(l.state == WAKES)
                            {
                                Guard g = guards[k];
                                for(int w = guards[k].lastWrite; w < l.timestamp.Minute; w++)
                                {
                                    g.sleepMap[w]++;
                                }
                                guards[k] = g;
                            }
                            else
                            {
                                Guard g = guards[k];
                                g.lastWrite = l.timestamp.Minute;
                                guards[k] = g;
                            }
                        }
                    }

                }
                else
                {
                    for(int k = 0; k < guards.Count; k++)
                    {
                        if(guards[k].id == history[i].guard)
                            newGuard = false;
                    }
                }

                if(newGuard)
                    guards.Add(new Guard(history[i].guard));

                Console.WriteLine(l.timestamp.ToShortDateString() + " " + l.timestamp.ToShortTimeString() + " - " + l.guard + " - " + l.state);
            }

            Console.WriteLine("----- guard        tIME -----");

            Console.WriteLine(guards.Count);

            //this is so so bad but im running out of time so...
            List<int> seen = new List<int>();

            foreach(Guard g in guards)
            {
                if(!seen.Contains(g.id))
                {
                    String mapVis = "";
                    int totalTime = 0;
                    int highestTime = 0;
                    int highestScore = 0;

                    for(int i = 0; i < 60; i++)
                    {
                        mapVis += g.sleepMap[i];
                        mapVis += "-";
                        totalTime += g.sleepMap[i];

                        if(g.sleepMap[i] > highestScore)
                        {
                            highestScore = g.sleepMap[i];
                            highestTime = i;
                        }

                    }


                    Console.WriteLine(g.id + " " + mapVis + " " + totalTime + " (" + highestTime + ", " + highestScore + ")");
                    seen.Add(g.id);
                }
            }

        }

        struct Log
        {
            public DateTime timestamp;
            public int guard;
            public int state;

            public Log(DateTime timestamp, int guard, int state)
            {
                this.timestamp = timestamp;
                this.guard = guard;
                this.state = state;
            }

        }

        struct Guard
        {
            public int id;
            public int sleepTime;

            public int[] sleepMap;
            public int writeCode;
            public int lastWrite;

            public Guard(int id)
            {
                this.id = id;
                this.sleepTime = 0;
                this.writeCode = 0;
                this.lastWrite = 0;
                this.sleepMap = new int[60];
            }

        }

    }
}
