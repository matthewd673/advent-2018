using System;
using System.Collections.Generic;
using System.IO;

class Program
{
	static String input = "";
	
	static int freq = 0;
	static List<int> freqs = new List<int>();

	static bool calibrated = false;

	public static void Main(String[] args)
	{

		freqs.Add(freq);

		input = File.ReadAllText("input.txt");

		String[] commands = input.Split('\n');

		while(!calibrated)
		{
			foreach(String c in commands)
			{
				freq += Convert.ToInt32(c);

				if(freqs.Contains(freq))
				{
					Console.WriteLine("Calibrated: " + freq);
					calibrated = true;
				}

				//Console.WriteLine(freqs.Count + " : " + freqs[freqs.Count - 1]);

				freqs.Add(freq);

			}

			Console.WriteLine("Result: " + freq);
			Console.WriteLine("Recorded: " + freqs.Count);

		}

	}
}