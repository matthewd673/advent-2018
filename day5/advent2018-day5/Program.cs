﻿using System;
using System.Collections.Generic;
using System.IO;

namespace advent2018_day5
{
    class Program
    {

        static string input;

        public static void Main(string[] args)
        {
            input = File.ReadAllText("input.txt");

            var watch = System.Diagnostics.Stopwatch.StartNew();
            partOne();
            Console.WriteLine(watch.ElapsedMilliseconds + "ms");

        }

        //this turned into parts 1 and 2, oh well
        public static void partOne()
        {
            char[] letterInput = input.ToCharArray();
            List<char> allLetters = new List<char>();

            foreach(char c in letterInput)
            {
                allLetters.Add(c);
            }

            char smallestChar = '_';
            int smallestCount = 500000;

            for(int k = 0; k < 26; k++)
            {
                //List<char> letters = allLetters;
                List<char> letters = new List<char>();
                foreach(char c in letterInput)
                    letters.Add(c);
                int removed = 0;
                for(int m = 0; m < letters.Count; m++)
                {
                    if(letters[m].ToString().ToUpper() == ((char)(65 + k)).ToString())
                    {
                        letters.RemoveAt(m);
                        m--;
                        removed++;
                    }
                }
                bool changed = true; //so the loop starts
                while(changed)
                {
                    changed = false;
                    for(int i = 0; i < letters.Count; i++)
                    {
                        //Console.Write(letters[i]);
                        //Console.Write(letters[i + 1]);
                        if(i < letters.Count - 1) //its safe to scan
                        {
                            if(letters[i].ToString().ToUpper() == letters[i + 1].ToString().ToUpper())
                            {
                                //its the same type
                                if(Convert.ToInt32(letters[i]) != Convert.ToInt32(letters[i + 1]))
                                {
                                    letters.RemoveAt(i + 1); //200 iq
                                    letters.RemoveAt(i);
                                    i--;
                                    changed = true;
                                }
                            }
                        }
                    }
                }
                Console.WriteLine((char)(65 + k) + ": " + letters.Count + " (" + removed + ")");
                if(smallestCount > letters.Count)
                {
                    smallestCount = letters.Count;
                    smallestChar = (char)(65 + k);
                }
            }

            Console.WriteLine("Smallest: " + smallestChar + " (" + smallestCount + ")");

            /*
            for(int i = 0; i < letters.Count; i++)
            {
                Console.Write(letters[i]);
            }
            Console.WriteLine();
            */

            //Console.WriteLine("length: " + allLetters.Count);
        }

    }
}