﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Day3Viz
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        String input;

        Claim[] claims;

        int maxX = 0;
        int maxY = 0;

        int[,] all;

        int maxVal = 0;
        int total = 0;

        private void Form1_Load(object sender, EventArgs e)
        {
            input = File.ReadAllText("input.txt");

            partOne();

        }

        public void partOne()
        {
            string[] cLines = input.Split('\n');
            claims = new Claim[cLines.Length];

            int totalArea = 0;

            

            for (int i = 0; i < cLines.Length; i++)
            {
                claims[i] = parseClaim(cLines[i]);
            }

            for(int i = 0; i < claims.Length; i++)
            {
                if (claims[i].x + claims[i].width > maxX)
                    maxX = claims[i].x + claims[i].width;
                if (claims[i].y + claims[i].height > maxY)
                    maxY = claims[i].y + claims[i].height;
            }

            all = new int[maxX, maxY];

            bool[,] skip = new bool[claims.Length, claims.Length];

            for(int i = 0; i < claims.Length; i++)
            {
                for(int k = claims[i].x; k < claims[i].width + claims[i].x; k++)
                {
                    for(int l = claims[i].y; l < claims[i].height + claims[i].y; l++)
                    {
                        all[k, l]++;
                    }
                }
            }            

            for(int i = 0; i < maxX; i++)
            {
                for(int j = 0; j < maxY; j++)
                {
                    if (all[i, j] > maxVal)
                        maxVal = all[i, j];
                    if (all[i, j] > 1)
                        total++;
                }
            }

            Invalidate();

        }

        public static bool detectCollision(Claim a, Claim b)
        {
            if (a.x < (b.x + b.width) &&
                (a.x + a.width) > b.x &&
                a.y < (b.y + b.height) &&
                (a.height + a.y) > b.y)
                return true;
            else
                return false;
        }

        public Claim parseClaim(String cString)
        {
            Claim c;

            string[] props = cString.Split(' ');
            int id = Convert.ToInt32(props[0].Split('#')[1]);

            string[] coords = props[2].Split(',');
            int x = Convert.ToInt32(coords[0]);
            int y = Convert.ToInt32(coords[1].Split(':')[0]);

            string[] dems = props[3].Split('x');
            int width = Convert.ToInt32(dems[0]);
            int height = Convert.ToInt32(dems[1]);

            c = new Claim(id, x, y, width, height);

            return c;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = CreateGraphics();
            Font font = new Font(FontFamily.GenericMonospace, 12);

            if (claims != null)
            {
                for (int i = 0; i < claims.Length; i++)
                {
                    g.DrawRectangle(Pens.Black, new Rectangle(claims[i].x, claims[i].y, claims[i].width, claims[i].height));
                    g.DrawString(claims[i].id.ToString(), font, Brushes.Red, new Point(claims[i].x, claims[i].y));
                }

                for (int i = 0; i < maxX; i++)
                {
                    for (int j = 0; j < maxY; j++)
                    {
                        if (all[i, j] > 1)
                            g.DrawRectangle(Pens.Green, new Rectangle(i, j, 1, 1));
                    }
                }

                g.DrawLine(Pens.Red, new Point(maxX, 0), new Point(maxX, maxY));
                g.DrawLine(Pens.Red, new Point(0, maxY), new Point(maxX, maxY));

                g.DrawString(total.ToString() + "\n" + maxVal.ToString(), font, Brushes.Orange, new Point(0, 0));

            }
            else
            {
                g.DrawString("i dont like this", font, Brushes.Black, new Point(0, 0));
            }
        }

        public struct Vec2
        {
            public int x, y;

            public Vec2(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public struct Claim
        {
            public int id;
            public int x, y, width, height;
            public Vec2 topLeft, botRight;

            public Claim(int id, int x, int y, int width, int height)
            {

                this.id = id;

                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;

                topLeft = new Vec2(x, y);
                botRight = new Vec2(x + width, y + height);

                //Console.WriteLine("{0}: {1},{2} & {3}x{4}", id, x, y, width, height);
           }

        }

    }
}
