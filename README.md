# Advent of Code 2018

This repository serves as a collection of my solutions for each day of Advent of Code 2018.
All solutions are written in some form of C# but in many different variants and environments including .NET Core, .NET Framework (+ WinForms), and an online REPL (repl.it).

**Disclaimer**

- This code is not optimized

- This code is not commented

- This code is not organized